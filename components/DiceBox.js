import React from "react";
import { StyleSheet, View, Text } from "react-native";
import Die from "./Die";
import uuid from "uuid";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "row",
    backgroundColor: "#fefefe",
    borderColor: "black",
    borderWidth: 1,
    borderRadius: 4,
    width: "98%",
    alignItems: "center",
    justifyContent: "flex-start",
    //Add a shadow under the box for iOS and Android
    shadowColor: "black",
    shadowOffset: { width: 0, height: 3 },
    shadowRadius: 10,
    shadowOpacity: 0.24,
    elevation: 12,
    margin: 4,
  },
});

export default function diceBox(props) {
  return (
    <View style={{ ...styles.container, ...props.style }}>
      {props.dice.map((die, index) => (
        <Die key={uuid()} id={index} onPress={props.onPress}>
          <Text>{die}</Text>
        </Die>
      ))}
    </View>
  );
}
