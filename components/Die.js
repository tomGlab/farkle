import React from "react";
import { StyleSheet, TouchableOpacity } from "react-native";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    maxWidth: "15.9%",
    marginHorizontal: "0.4%",
    aspectRatio: 1,
    backgroundColor: "#dedede",
    borderColor: "black",
    borderWidth: 1,
    borderRadius: 6,
    alignItems: "center",
    justifyContent: "center",
    // //Add a shadow under the box for iOS and Android
    shadowColor: "black",
    shadowOffset: { width: 0, height: 3 },
    shadowRadius: 8,
    shadowOpacity: 0.32,
    elevation: 8,
  },
});

export default function Die(props) {
  return (
    <TouchableOpacity style={{ ...styles.container, ...props.style }} onPress={() => props.onPress(props.id)}>
      {props.children}
    </TouchableOpacity>
  );
}
