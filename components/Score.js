//Take an array of 1-6 numbers. Determine if there are any points in that set. Return the Ordered set of points, and the points in an object.

//Some examples below:
// dice: [ 2, 1, 3, 4, 1, 5 ]
// score {
//  total: 250,
//  sets: [
//    { points: 100, dice: [Array] },
//    { points: 100, dice: [Array] },
//    { points: 50, dice: [Array] }
//  ],
//  unused: [ 0, 2, 3 ]
// }

// dice: [ 2, 1, 3, 4, 6, 5 ]
// score { total: 1500, sets: [ { points: 1500, dice: [Array] } ], unused: [] }

// dice: [ 2, 6, 6, 5, 1, 6 ]
// score {
//  total: 750,
//  sets: [
//    { points: 600, dice: [Array] },
//    { points: 100, dice: [Array] },
//    { points: 50, dice: [Array] }
//  ],
//  unused: [ 0 ]
// }

function pointsCalc(dice) {
  var sets = new Array();
  for (i = 0; i <= 6; i++) {
    sets[i] = { count: 0, indices: [] };
  }
  
  let score = {
    total: 0,
    sets: [],
    unused: [],
  };

  dice.forEach((value, index) => {
    sets[value].count++;
    sets[value].indices.push(index);
  });

  //6 of a kind, 3000 points.
  setOf6 = sets.findIndex((e) => {
    return e.count == 6;
  });
  if (setOf6 > 0) {
    let set = sets[setOf6];
    sets[setOf6] = { count: 0, indices: [] };
    let points = 3000;
    score.total += points;
    score.sets.push({ points, dice: set.indices });
  }

  //2 triplets, 2500 points
  triplets1 = sets.findIndex((e) => {
    return e.count == 3;
  });
  triplets2 = sets.findIndex((e, i) => {
    return i != triplets1 && e.count == 3;
  });
  if (triplets1 > 0 && triplets2 > 0) {
    let triplets1Values = sets[triplets1];
    let triplets2Values = sets[triplets2];
    sets[triplets1Values] = { count: 0, indices: [] };
    sets[triplets2Values] = { count: 0, indices: [] };

    let points = 2500;

    let diceIndices = new Array();
    diceIndices.concat(triplets1Values);
    diceIndices.concat(triplets2Values);

    score.total += points;
    score.sets.push({ points, dice: diceIndices });
  }

  //5 of a kind, 2000 points
  setOf5 = sets.findIndex((e) => {
    return e.count == 5;
  });
  if (setOf5 > 0) {
    let set = sets[setOf5];
    sets[setOf5] = { count: 0, indices: [] };

    let points = 2000;
    score.total += points;
    score.sets.push({ points, dice: set.indices });
  }

  //Set of 4 and set of 2, 1500 points
  setOf4 = sets.findIndex((e) => {
    return e.count == 4;
  });
  setOf2 = sets.findIndex((e) => {
    return e.count == 2;
  });
  if (setOf4 > 0 && setOf2 > 0) {
    let setof4Values = sets[setOf4];
    let setof2Values = sets[setOf2];
    sets[setOf4] = { count: 0, indices: [] };
    sets[setOf2] = { count: 0, indices: [] };
    let points = 1500;

    let diceIndices = new Array();
    diceIndices.concat(setof4Values);
    diceIndices.concat(setof2Values);

    score.total += points;
    score.sets.push({ points, dice: diceIndices });
  }

  //3 pairs, 1500 points
  pair1 = sets.findIndex((e) => {
    return e.count == 2;
  });
  pair2 = sets.findIndex((e, i) => {
    return i != pair1 && e.count == 2;
  });
  pair3 = sets.findIndex((e, i) => {
    return i != pair1 && i != pair2 && e.count == 2;
  });
  if (pair1 > 0 && pair2 > 0 && pair3 > 0) {
    let pair1Values = sets[pair1];
    let pair2Values = sets[pair2];
    let pair3Values = sets[pair3];
    sets[pair1] = { count: 0, indices: [] };
    sets[pair2] = { count: 0, indices: [] };
    sets[pair3] = { count: 0, indices: [] };
    let points = 1504;

    let diceIndices = new Array();
    diceIndices.concat(pair1Values);
    diceIndices.concat(pair2Values);
    diceIndices.concat(pair3Values);

    score.total += points;
    score.sets.push({ points, dice: diceIndices });
  }

  //Run of 6 (1-6), 1500 points.
  //Going to remove what is required for a run. Even though the Farkle offically doesn't have more then 6 dice.
  let hasRun = sets.every((e, i) => {
    if (i == 0) {
      //As dice are stored 1-6, sets[0] is always empty.
      return true;
    }
    return e.count > 0;
  });
  if (hasRun) {
    let points = 1500;
    var rundice = [];
    sets.forEach((e, i) => {
      if (i) {
        e.count--;
        let useddice = e.indices.splice(0, 1); //need to add this to an array of new dice to push in to and keep record of!
        rundice.push(...useddice);
      }
    });
    score.total += points;
    score.sets.push({ points, dice: rundice });
  }
  //???

  //4 of a kind, 1000 points
  setOf4 = sets.findIndex((e) => {
    return e.count == 4;
  });
  if (setOf4 > 0) {
    let set = sets[setOf4];
    sets[setOf4] = { count: 0, indices: [] };
    let points = 1000;
    score.total += points;
    score.sets.push({ points, dice: set.indices });
  }

  //3 of a kind, points depend on the dice value.
  setOf3 = sets.findIndex((e) => {
    return e.count == 3;
  });
  if (setOf3 > 0) {
    let set = sets[setOf3];
    sets[setOf3] = { count: 0, indices: [] };
    let points = setOf3 == 1 ? 300 : setOf3 * 100;
    score.total += points;
    score.sets.push({ points, dice: set.indices });
  }

  //1's, 100 points each
  if (sets[1].count) {
    let points = 100;
    let diceValues = sets[1];
    sets[1] = { count: 0, indices: [] };
    diceValues.indices.forEach((diceIndice) => {
      score.total += points;
      score.sets.push({ points, dice: [diceIndice] });
    });
  }
  //5's 50 points each
  if (sets[5].count) {
    let points = 50;
    let diceValues = sets[5];
    sets[5] = { count: 0, indices: [] };
    diceValues.indices.forEach((diceIndice) => {
      score.total += points;
      score.sets.push({ points, dice: [diceIndice] });
    });
  }
  sets.forEach((e) => {
    score.unused.push(...e.indices);
  });
  return score;
}
