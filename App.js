import React, { useState, useEffect } from "react";
import {
  StyleSheet,
  Text,
  SafeAreaView,
  StatusBar,
  ScrollView,
  View,
  TouchableOpacity,
} from "react-native";
import DiceBox from "./components/DiceBox";
import uuid from "uuid";

export default function App() {
  const [diceHistory, setDiceHistory] = useState([]);
  const [savedRoll, setSavedRoll] = useState([]);
  const [currentRoll, setCurrentRoll] = useState([]);

  const [testingPopulation, setTestingPopulation] = useState(
    Math.floor(Math.random() * 6) + 2
  );

  const handleKeep = () => {
    setDiceHistory(diceHistory.concat([savedRoll]));
    setSavedRoll([])
  };
  const handleRoll = () => {
    let newDice = [];
    for (rollInt = currentRoll.length || 6; rollInt > 0; rollInt--) {
      newDice.push(Math.floor(Math.random() * 6) + 1);
    }

    setCurrentRoll(newDice);
  };

  const handlePressDieFromSaved = (e) => {
    console.log("handlePressDieFromSaved", e);
    setSavedRoll(savedRoll.slice(0, e).concat(savedRoll.slice(e + 1)));
    setCurrentRoll(currentRoll.concat(savedRoll[e]))

  };
  const handlePressDieFromCurrent = (e) => {
    console.log("handlePressDieFromCurrent", e);
    setCurrentRoll(currentRoll.slice(0, e).concat(currentRoll.slice(e + 1)));
    setSavedRoll(savedRoll.concat(currentRoll[e]))
  };

  // useEffect(() => {
  //   testingPopulation > 0 && setTestingPopulation(testingPopulation - 1);
  //   handleRoll();
  //   handleKeep();
  // }, [testingPopulation]);

  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.rolledHistory}>
        <ScrollView>
          {diceHistory.map(
            (diceSet) =>
              diceSet.length > 0 && <DiceBox key={uuid()} dice={diceSet} />
          )}
          {savedRoll.length > 0 && (
            <DiceBox
              key={uuid()}
              dice={savedRoll}
              onPress={handlePressDieFromSaved}
            />
          )}
        </ScrollView>
      </View>
      <View style={styles.currentRoll}>
        {currentRoll.length > 0 && (
          <DiceBox
            key={uuid()}
            dice={currentRoll}
            onPress={handlePressDieFromCurrent}
          />
        )}
        {currentRoll.length <= 0 && <Text> Please Roll new dice!</Text>}
        <View style={styles.buttonContainer}>
          <TouchableOpacity style={styles.turnButtons} onPress={handleRoll}>
            <Text>Roll</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.turnButtons} onPress={handleKeep}>
            <Text>Keep</Text>
          </TouchableOpacity>
        </View>
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: StatusBar.currentHeight,
    backgroundColor: "#fff",
  },
  rolledHistory: {
    flex: 8,
    alignItems: "stretch",
    borderWidth: 1,
    marginBottom: 5,
    borderColor: "pink",
  },
  currentRoll: {
    flex: 3,
    alignItems: "stretch",
    borderWidth: 1,
    borderColor: "pink",
  },
  buttonContainer: {
    flex: 1,
    flexDirection: "row",
    alignContent: "space-between",
    alignItems: "center",
    borderWidth: 1,
    borderColor: "pink",
  },
  turnButtons: {
    flex: 2,
    height: "100%",
    alignContent: "center",
    alignItems: "center",
    borderWidth: 1,
    borderColor: "pink",
    backgroundColor: "blue",
  },
});
